from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=500, unique=True)
    closet_name = models.CharField(max_length=300)
    bin_number = models.PositiveIntegerField(default=0)
    bin_size = models.PositiveIntegerField(default=0)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=300)
    model_name = models.CharField(max_length=300)
    color = models.CharField(max_length=300)
    picture_url = models.URLField(null=True)
    created = models.DateTimeField(auto_now_add=True)

    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse('api_show_shoe', kwargs={'pk': self.pk})

    class Meta:
        ordering = ("manufacturer", "model_name")
