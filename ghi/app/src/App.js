import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesMain from './shoes/ShoesMain';
import Bin from './shoes/Bin';
import ShoeForm from './shoes/ShoeForm';
import ShoeList from './shoes/ShoeList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesMain />}>
          </Route>
          <Route path="/shoes/:id" element={<Bin />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/shoes/list" element={<ShoeList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
