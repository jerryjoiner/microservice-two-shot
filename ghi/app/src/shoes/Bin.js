import React, { cloneElement } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';


function Bin(props) {
  return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-white">
          <h1 className="display-5 fw-bold">BIN TITLE</h1>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add New Shoes</Link>
            <Link to="/shoes/list" className="btn btn-success btn-lg px-4 gap-3">All Shoes</Link>
          </div>
        </div>
        <div className="container">
          <h2>Current Shoes</h2>
          <div className="row">
            <div className="col">
              UNDER CONSTRUCTION: Please check back later
            </div>
          </div>
        </div>
      </>
  );
}

export default Bin;
