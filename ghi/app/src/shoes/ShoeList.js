import { useEffect , useState} from "react";
import React from 'react';

function ShoeList() {

    const [shoes, setShoes] = useState([]);

    useEffect(() => {
        const url = 'http://localhost:8080/api/shoes'
        fetch(url)
        .then(response => {
                if(response.ok) {
                    let data = response.json()
                    return data
                }
                throw new Error ("BAD RESPONSE")

        })
        .then(data => {setShoes(data.shoes)})
        .catch((err) => console.log(err))
    }, [])


    function deleteShoe(id) {
        const shoeUrl = `http://localhost:8080/api/shoes/${id}/`
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(shoeUrl, fetchConfig)
        .then((response) => {
            if (!response.ok) {
                throw new Error ("Something went wrong!")
            }
        })
        .then(setShoes(shoes.filter(shoe => shoe.id !== id)))
        .catch((err) => {
            console.log(err)
        })
    }

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Picture</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Delete</th>

          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return(
                <tr key={shoe.href}>
                <td><img src={shoe.picture_url} className="img-fluid img-thumbnail" /></td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>{shoe.bin}</td>
                <td>
                    <button type="button" className="btn btn-danger" onClick={() => deleteShoe(shoe.id)}>DELETE</button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    );
  }

  export default ShoeList;
