import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: '',
            bins: []
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();

          this.setState({bins: data.bins});

        }
      }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({[name]: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.bins;
        console.log(data);

        const binId = data.bin

        const shoeUrl = `http://localhost:8080${binId}shoes/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            const cleared = {
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
              };
              this.setState(cleared);
        }
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a New Shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Manufacturer" required
                                type="text" name ="manufacturer" id="manufacturer"
                                className="form-control" value={this.state.manufacturer} />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Model"
                                required type="text" name="model_name"  id="model_name"
                                className="form-control" value={this.state.model_name} />
                                <label htmlFor="model_name">Model</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Color"
                                required type="text" name="color"  id="color"
                                className="form-control" value={this.state.color} />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} placeholder="Picture Url"
                                required type="url" name="picture_url"  id="picture_url"
                                className="form-control" value={this.state.picture_url} />
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleInputChange} required
                                name="bin" id="bin"
                                className="form-select" value={this.state.bin} >
                                    <option value="">Choose a Bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.href}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoeForm;
