import React, { cloneElement } from 'react';
import { Link } from 'react-router-dom';

function BinsColumn(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        return (
          <Link to={`/shoes/${data.id}`} key={data.href} className="text-decoration-none text-reset">
            <div className="card mb-3 shadow">
              <img src={data.picture_url} className="card-img-top figure-img img-fluid" />
              <div className="card-body">
                <h5 className="card-title">{data.closet_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                  Bin Size: {data.bin_size}
                </h6>
                <p className="card-text">

                </p>
              </div>
            </div>
          </Link>
        );
      })}
    </div>
  );
}

class ShoesMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      binColumns: [[], [], []],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();

        const requests = [];
        for (let bin of data.bins) {
          const detailUrl = `http://localhost:8100${bin.href}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const binColumns = [[], [], []];

        let i = 0;
        for (const binResponse of responses) {
          if (binResponse.ok) {
            const details = await binResponse.json();
            binColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(binResponse);
          }
        }

        this.setState({binColumns: binColumns});
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-white">
          <h1 className="display-5 fw-bold">SHOES!</h1>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add New Shoes</Link>
            <Link to="/shoes/list" className="btn btn-success btn-lg px-4 gap-3">All Shoes</Link>
          </div>
        </div>
        <div className="container">
          <h2>Current Bins</h2>
          <div className="row">
            {this.state.binColumns.map((binList, index) => {
              return (
                <BinsColumn key={index} list={binList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default ShoesMain;
