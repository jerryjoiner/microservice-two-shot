# Wardrobify

Team:

* Person 1 - Which microservice?
* Allen Chen - Shoes

## Design

DIAGRAM: /Users/droid/hrc/practiceproj/microservice-two-shot/ghi/app/public/Wardrobe_diagram.png

## Shoes microservice

Wardrobe: Bins and Locations
    Locations:
        - closet_name: name of hat closet; no more than 100 characters
        - section_number: unique section ID
        - shelf_number: the number of hats that can exist in this "Location"

    Bins:
        - closet_name: name of shoe closet; no more than 100 character
        - bin_number: unqiue bin ID
        - bin_size: the number of shoes that can exist in this "Bin"
        - picture_url: url of the bin logo


Shoes: Shoes can be placed into a Bin from Wardrope API
    views.py: view functions
        - BinVODetailEncoder: encoder referencing data from Wardrobe-Bin
        - ShoeListEncoder: encoder referencing data from Shoe model
        - ShoeDetailEncoder: encoder referencing data from Shoe Model

        - def api_list_shoes: route GET or POST requests
            - GET: Either fetch shoes in a specific bin based on bin_number or the entire list of shoe
                - URL path for all shoes: http://localhost:8080/api/shoes
                - URL path for shoes in specific bin: http://localhost:8080/api/bins/<int:pk>/shoes/
            - POST: Add a new shoe to the database
                - URL path to add a new shoe: http://localhost:8080/api/bins/<int:pk>/shoes/ name="api_list_shoes",),
        - def api_show_shoe: route GET or DELETE requests
            - GET: fetch a specific shoe based on unique shoe id
                - URL path to get a specific shoe: http://localhost:8080/api/shoes/<int:pk>/
            - DELETE: delete a shoe based on unique shoe id
                - URL path to delete a specific shoe: http://localhost:8080/api/shoes/<int:pk>/

Front-End Navigation:
    Nav:
        Home: Takes user to home page
        Shoes: Takes user to Shoes Main page
            - Add New Shoes: Takes user to add a new shoe form page
                - click on create to add new shoe
            - All Shoes: Takes user to view all shoes
                - click on delete to delete specific shoes
        Bins: Click on individual bins to go to a page with shoes for that bin
            Currently under construction


Value Objects:
    In the context of the bin and shoe relationship. The shoe is the entity object and bin is the value object. A shoe individually can be viewd as an entity object since it is unique and immutable. Since a shoe can be added or removed from a bin. The bin is mutable for a shoe.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
